package pl.edu.pjatk.mpr.lab5.service;

import com.sun.org.apache.xpath.internal.operations.Or;
import com.sun.org.apache.xpath.internal.operations.String;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class OrdersService {

    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
        orders.stream().filter(order -> order.getItems().size() > 5);
        return orders;
    }

    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
        ClientDetails theOldestClient = orders.stream().map(order -> order.getClientDetails()).max((Comparator.comparing(ClientDetails::getAge))).get();
        return theOldestClient;
    }

    public static Order findOrderWithLongestComments(List<Order> orders) {
        Order withLongestComments = orders.stream()
                .max(Comparator.comparing(Order::getComments))
                .get();
        return withLongestComments;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
        Order above18YO = orders.stream().
                map(order -> order.getClientDetails()).filter(ClientDetails -> ClientDetails.getAge() > 18)
                .map(n -> { String name = n.getName() + ",";
                            String surname = n.getSurname() + ",";
                            String tmp = name + surname;
                            return  tmp;
                });
    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
        List<String> OrderItemsNames = orders.stream().filter(comment -> comment.getComments().startsWith("A"))
                .map(item -> item.getItems())
                .flatMap(Collection::stream)
                .sorted((p1, p2) -> p1.getName().compareTo(p2.getName()))
                .map(item -> item.getName().toString())
                .collect(Collectors.toList());


        return OrderItemsNames;
    }

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
        orders.stream()
                .map(order -> order.getClientDetails()).
                filter(client -> client.getLogin().toUpperCase().matches("^S.*")).
                forEach(System.out::println);
    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
        Map<ClientDetails, List<Order>> groupByClient = orders.stream().collect(Collectors.groupingBy(Order::getClientDetails));
        return groupByClient;
    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        Map<Boolean, List<ClientDetails>> partitionedByAge = orders.stream().map(order -> order.getClientDetails()).collect(Collectors.partitioningBy(client -> client.getAge() > 18));
        return partitionedByAge;
    }

}
